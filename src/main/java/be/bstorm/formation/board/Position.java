package be.bstorm.formation.board;

import java.util.ArrayList;
import java.util.List;

public record Position(int x, int y) {

    public Position(Position position) {
        this(position.x(), position.y());
    }
    public List<Position> getNearbyPosition(int width, int height) {
        List<Position> positions = new ArrayList<>();

        for(int line = this.y - 2; line < this.y + 2; line++) {
            for(int col = this.x - 2; col < x + 2; col++) {
                Position target = new Position(col, line);
                if (this.equals(target)) continue;
                if (col >= 0 && col < width && line >= 0 && line < height) {
                    positions.add(target);
                }
            }
        }

        return positions;
    }
}
