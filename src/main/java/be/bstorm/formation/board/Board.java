package be.bstorm.formation.board;

import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

public class Board implements Iterable<CellType> {
    private int width;
    private int height;

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    private CellType[][] cells;

    public Board(int width, int height) {
        this.cells = new CellType[height][width];
        this.width = width;
        this.height = height;
    }

    public CellType getCell(Position position) {
        return getCell(position.x(), position.y());
    }
    public CellType getCell(int x, int y) {
        return this.cells[y][x];
    }

    public void initBoard() {
        for(int i = 0; i < height; i++) {
            for(int j = 0; j < width; j++) {
                cells[i][j] = CellType.EMPTY;
            }
        }
    }

    public boolean addCharacter(Position position, CellType cellType) {
        CellType targetCell = this.cells[position.y()][position.x()];
        if (targetCell != CellType.EMPTY) return false;
        if (cellType == CellType.MONSTER) {
            List<Position> nearby = position.getNearbyPosition(this.width, this.height);

            boolean inserted = nearby.stream()
                    .map(p -> cells[p.y()][p.x()])
                    .anyMatch(ct -> ct != CellType.EMPTY);
            if (!inserted) {
                this.cells[position.y()][position.x()] = cellType;
            }
            return !inserted;
        }
        this.cells[position.y()][position.x()] = cellType;
        return true;
    }
    public void setEmpty(Position position) {
        this.cells[position.y()][position.x()] = CellType.EMPTY;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < width; i++) {
            builder.append("====");
        }
        builder.append("\n");
        int col = 0;
        builder.append("|");
        for(CellType cellType: this) {
            builder.append(cellType.type).append("|");
            col++;
            if (col >= width) {
                col = 0;
                builder.append("\n").append("|");
            }
        }
        for (int i = 0; i < width; i++) {
            builder.append("====");
        }
        return builder.toString();
    }

    @Override
    public Iterator<CellType> iterator() {
        return new BoardIterator();
    }

    private class BoardIterator implements Iterator<CellType> {
        private int line = 0;
        private int col = 0;

        @Override
        public boolean hasNext() {
            return height > line && line >= 0 && width > col && col >= 0;
        }

        @Override
        public CellType next() {
            CellType type = cells[line][col++];
            if (col >= width) {
                line++;
                col = 0;
            }
            return type;
        }
    }
}
