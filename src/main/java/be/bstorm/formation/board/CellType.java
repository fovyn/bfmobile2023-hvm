package be.bstorm.formation.board;

public enum CellType {
    EMPTY(" - "),
    HERO(" H "),
    MONSTER(" M ");

    public String type;

    CellType(String type) {
        this.type = type;
    }
}
