package be.bstorm.formation.characters;

public enum CharacterState {
    LIVE,
    DEAD,
    LOOTER;
}
