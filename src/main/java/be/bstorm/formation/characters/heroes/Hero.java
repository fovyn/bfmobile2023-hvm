package be.bstorm.formation.characters.heroes;

import be.bstorm.formation.characters.Character;
import be.bstorm.formation.inventories.Inventory;

public abstract class Hero extends Character {
    public Hero(int stamina, int strength) {
        super(stamina, strength);
    }

    public void loot(Inventory inventory) {
        inventory.forEach(this.getInventory()::add);
    }

    public void restore() {
        this.setCurrentLp(this.getLp());
    }
}
