package be.bstorm.formation.characters;

import be.bstorm.formation.board.Position;
import be.bstorm.formation.inventories.Inventory;
import be.bstorm.formation.inventories.ItemType;
import be.bstorm.formation.utils.DiceType;

public abstract class Character {
    private int staminaClassModifier;
    private int strengthClassModifier;
    private int stamina;
    private int strength;
    private int lp;
    private int currentLp;
    protected CharacterState state;

    public int getLp(){ return this.lp; }
    protected void setCurrentLp(int lp) {
        this.currentLp = lp;
    }

    private Inventory inventory = new Inventory();
    private Position position;

    public Character(int stamina, int strength) {
        this.staminaClassModifier = stamina;
        this.strengthClassModifier = strength;

        int staminaRnd = DiceType.D6.rollStat();
        int strengthRnd = DiceType.D6.rollStat();

        this.stamina = staminaRnd + getModifier(staminaRnd + stamina);
        this.strength = strengthRnd + getModifier(strengthRnd + strength);

        this.lp = this.stamina + getModifier(this.stamina) + this.staminaClassModifier;
        this.currentLp = this.lp;

        this.state = CharacterState.LIVE;
    }

    public void move(Position position) {
        this.position = position;
    }
    public Position getPosition() {
        return new Position(position.x(), position.y());
    }
    public CharacterState getState() {
        return this.state;
    }

    public boolean isDead() {
        return this.state.equals(CharacterState.DEAD) || this.state.equals(CharacterState.LOOTER);
    }
    private int getModifier(int stat) {
        int modifier = -1;

        if (stat > 15) modifier = 2;
        else if (stat > 10) modifier = 1;
        else if (stat > 5) modifier = 0;

        return modifier;
    }

    public void hit(Character character) {
        int dmg = DiceType.D4.roll() + this.getModifier(this.strength + this.strengthClassModifier);

        character.setDmg(dmg);
    }

    private void setDmg(int dmg) {
        this.currentLp -= dmg;
        if (this.currentLp <= 0) {
            this.state = CharacterState.DEAD;
        }
    }

    protected void addItem(ItemType type, int qtt) {
        this.inventory.add(type, qtt);
    }

    protected Inventory getInventory() {
        return this.inventory;
    }

    @Override
    public String toString() {
        return "Character{" +
                "type=" + this.getClass().getSimpleName() +
                ", state="+ state +
                ", stamina=" + stamina +
                ", strength=" + strength +
                ", lp=" + lp +
                ", currentLp=" + currentLp +
                ", inventory=" + inventory +
                '}';
    }
}
