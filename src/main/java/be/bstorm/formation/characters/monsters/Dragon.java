package be.bstorm.formation.characters.monsters;

public class Dragon extends Monster {
    public Dragon() {
        super(1, 0);

        this.initGold();
        this.initLeather();
    }
}
