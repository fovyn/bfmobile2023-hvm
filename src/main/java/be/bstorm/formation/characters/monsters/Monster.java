package be.bstorm.formation.characters.monsters;

import be.bstorm.formation.characters.Character;
import be.bstorm.formation.characters.CharacterState;
import be.bstorm.formation.inventories.Inventory;
import be.bstorm.formation.inventories.ItemType;
import be.bstorm.formation.utils.DiceType;

public abstract class Monster extends Character {

    public Monster(int stamina, int strength) {
        super(stamina, strength);
    }

    protected void initLeather() {
        int leather = DiceType.D4.roll();
        this.addItem(ItemType.LEATHER, leather);
    }
    protected void initGold() {
        int gold = DiceType.D6.roll();
        this.addItem(ItemType.GOLD, gold);
    }

    public Inventory drop() {
        this.state = CharacterState.LOOTER;
        return this.getInventory();
    }
}
