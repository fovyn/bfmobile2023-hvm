package be.bstorm.formation.utils;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public enum DiceType {
    D4(1,4),
    D6(1,6);

    private int min;
    private int max;

    DiceType(int min, int max) {
        this.max = max;
        this.min = min;
    }

    public int roll() {
        Random random = new SecureRandom();

        return random.nextInt(min, max + 1);
    }

    public int rolls(int nbDice) {
        return rolls(nbDice, nbDice);
    }
    public int rolls(int nbDice, int nbKeep) {
        List<Integer> rolls = new ArrayList<>(nbDice);

        for(int i = 0; i < nbDice; i++) {
            rolls.add(roll());
        }

        return rolls.stream()
                .mapToInt(i -> i)
                .unordered()
                .limit(nbKeep)
                .sum();
    }

    public int rollStat() {
        return rolls(4,3);
    }
}
