package be.bstorm.formation.inventories;

public enum ItemType {
    GOLD,
    LEATHER;
}
