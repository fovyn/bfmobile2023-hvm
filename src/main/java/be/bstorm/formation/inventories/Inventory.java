package be.bstorm.formation.inventories;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Inventory implements Iterable<Map.Entry<ItemType, Integer>> {
    private HashMap<ItemType, Integer> items = new HashMap<>();

    public void add(ItemType type, int qtt) {
        int total = 0;
        if (this.items.containsKey(type)) {
            total = this.items.get(type);
        }
        this.items.put(type, total + qtt);
    }

    public void add(Map.Entry<ItemType, Integer> item) {
        this.add(item.getKey(), item.getValue());
    }

    public void plus(Inventory inventory) {

    }

    @Override
    public Iterator<Map.Entry<ItemType, Integer>> iterator() {
        return items.entrySet().iterator();
    }

    @Override
    public String toString() {
        return "Inventory{" +
                "items=" + items +
                '}';
    }
}
