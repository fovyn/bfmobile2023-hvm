package be.bstorm.formation;

import be.bstorm.formation.board.Board;
import be.bstorm.formation.board.CellType;
import be.bstorm.formation.board.Position;
import be.bstorm.formation.characters.Character;
import be.bstorm.formation.characters.heroes.Hero;
import be.bstorm.formation.characters.heroes.Human;
import be.bstorm.formation.characters.monsters.Dragon;
import be.bstorm.formation.characters.monsters.Monster;
import be.bstorm.formation.characters.monsters.Orc;
import be.bstorm.formation.characters.monsters.Wolf;
import be.bstorm.formation.commands.Direction;
import be.bstorm.formation.commands.MoveCommand;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Main {
    static Hero hero = new Human();
    static Board board = new Board(15,15);
    static List<Character> characters = new ArrayList();
    public static void main(String[] args) {
        board.initBoard();

        Random random = new SecureRandom();
        int x = random.nextInt(0, 15);
        int y = random.nextInt(0, 15);
        board.addCharacter(new Position(x, y), CellType.HERO);
        hero.move(new Position(x, y));

        int nbMonster = 0;
        do {
            x = random.nextInt(0, 15);
            y = random.nextInt(0, 15);
            boolean inserted = board.addCharacter(new Position(x,y), CellType.MONSTER);
            if (inserted) {

                int rnd = random.nextInt(0, 3);

                Monster monster = switch (rnd) {
                    case 0 -> new Wolf();
                    case 1 -> new Orc();
                    default -> new Dragon();
                };

                monster.move(new Position(x,y));
                characters.add(monster);
                nbMonster++;
            }
        } while (nbMonster < 10);

        System.out.println(board);

        MoveCommand command = new MoveCommand(hero, board, Main::fight);

        while (!hero.isDead()) {
            Position oldPosition = hero.getPosition();
            Direction direction = Direction.readKey();
            boolean hasMoved = command.execute(direction);
            if (hasMoved) {
                Position newPosition = hero.getPosition();
                board.setEmpty(oldPosition);
                board.addCharacter(newPosition, CellType.HERO);
            }
            System.out.println(board);
        }
    }

    private static boolean fight(Position position) {
        Character target = characters.stream()
                .filter(character -> character.getPosition().equals(position))
                .findFirst()
                    .orElseThrow(() -> new RuntimeException("NoTarget"));
        if (target instanceof Monster monster) {
            while (!hero.isDead() && !monster.isDead()) {
                hero.hit(monster);
                if (monster.isDead()) {
                    hero.loot(monster.drop());
                    hero.restore();
                } else {
                    monster.hit(hero);
                }
            }

            return monster.isDead();
        }
        return false;
    }
}