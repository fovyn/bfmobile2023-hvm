package be.bstorm.formation.commands;

import be.bstorm.formation.board.Board;
import be.bstorm.formation.board.CellType;
import be.bstorm.formation.board.Position;
import be.bstorm.formation.characters.Character;

import java.util.function.Function;

public class MoveCommand {
    private Character character;
    private Board board;
    private Function<Position, Boolean> fightAction;

    public MoveCommand(Character character, Board board, Function<Position, Boolean> fightAction) {
        this.character = character;
        this.board = board;
        this.fightAction = fightAction;
    }

    private boolean canMove(Position target) {
        int x = target.x();
        int y = target.y();

        if (x >= this.board.getWidth() || x < 0) return false;
        if (y >= this.board.getHeight() || y < 0) return false;

        CellType targetCell = this.board.getCell(target);
        if (targetCell.equals(CellType.MONSTER)) {
            return fightAction.apply(target);
        }
        return targetCell.equals(CellType.EMPTY);
    }

    public boolean execute(Direction direction){
        int x = character.getPosition().x();
        int y = character.getPosition().y();

        Position newPosition = switch (direction) {
            case NORTH -> new Position(x, y - 1);
            case EAST -> new Position(x + 1, y);
            case WEST -> new Position(x - 1, y);
            case SOUTH -> new Position(x, y + 1);
        };

        boolean canMove = canMove(newPosition);
        if (canMove) {
            character.move(newPosition);
        }

        return canMove;
    }

}
