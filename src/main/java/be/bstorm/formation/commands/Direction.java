package be.bstorm.formation.commands;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public enum Direction {
    NORTH("z"),
    EAST("d"),
    SOUTH("s"),
    WEST("q");

    private String key;

    Direction(String key) {
        this.key = key;
    }

    public static Direction readKey() {
        Direction[] directions = Direction.values();
        List<String> authorizeKey = Arrays.stream(directions).map(it -> it.key).toList();
        Scanner scanner = new Scanner(System.in);
        System.out.println(scanner.hasNextLine());
        String read = scanner.nextLine();
        int index = authorizeKey.indexOf(read);
        while (read != null && index == -1) {
//            scanner = scanner.reset();
            read = scanner.nextLine();
            index = authorizeKey.indexOf(read);
        }
//        scanner.close();

        return directions[index];
    }
}
